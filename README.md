# Overview

This `parent` project stores design information common to all of the projects
within the `hotspot_internet` GitLab group.

## Purpose

`hotspot_internet` refers to using the hotspot feature of cellular lines
to allow for a LAN to access the Internet. This includes the following
general setup:

* 1+ SIM cards
    * Line associated with each card has a limited amount of data (_e.g._ 30 GB/month)
    * Each card installed in a mobile router
* A multi-wan router for aggregating the multiple hotspot connections
* Optional: A WAP connected to multi-wan router to connect LAN hosts to Internet

## Requirements

This is the initial, high-level set of requirements for the entire hotspot Internet
system.

* Display data usage status over billing cycle
    * cumulative
    * remaining
    * total and per SIM card basis
* Deactivate hotspot whose data limit has been reached
* Reactivate hotspots
    * when billing cycle resets
    * when all hotspots have reached data limit
* Notifications
    * When data usage (for a given period, _e.g._ day, hour, etc.) exceeds a threshold
    * When hotspot is disabled and re-enabled

## Details

See design documents in artifacts of this project for additional details.
