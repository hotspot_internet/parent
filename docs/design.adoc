= Hotspot Internet Design

This document describes high-level features/design of the hotspot Internet system.
See README.md in https://gitlab.com/hotspot_internet/parent/[Hotspot Internet Parent Project]
for purpose and background of the system in general.

== Overview

[[hierarchical-diagram]]
.High-level architecture.
[ditaa]
----
                                      +--------------+  +-------------+
                                      |email delivery|  | usage email |
                                      |   service    |  |  recipient  |
                                      +--------------+  +-------------+
                                          ^                    ^
                                          |                    | 
             +---------------------------------------------------------------+
             |                            |                    |             |
             |                            V                    |             |
             |                         +--------------+        |             |
             |                         |notifications |<-------+             |
             |                         +--------------+                      |
             |                            |                                  |
             |                            |                                  |
+--------+   |                            V                                  |
|  web   |   |   +--------------+    +--------------+    +--------------+    |
| client |<--|-->| web server   |<-->| REST service |<-->|   database   |    |
|        |   |   |      +       |    |              |    |              |    |
+--------+   |   | web frontend |    +--------------+    +--------------+    |
             |   +--------------+        ^                                   |
             |                           |                                   |
             |                           V      backend                      |
             +----------------------------+----------------------------------+
                                          ^
                                          |
                                          V
                                 +------------------------+
                                 |        ^               |
                                 |        |               |
                                 |        V               |
                                 |    +---------------+   |
                                 |    | router driver |   |
                                 |    +---------------+   |
                                 |        ^               |
                                 |        |               |
                                 |        V               |
                                 |    +---------------+   |
                                 |    |     router    |   |
                                 |    +---------------+   |
                                 |                        |
                                 |                LAN     |
                                 +------------------------+
----

== Components

=== router

This is the hardware that aggregates multiple hotspot connections and provides
the LAN with an Internet connection. It has the following features:

* main source of data that the web server will provide to clients
** expected to have information about the total data transfered to/from each hotspot connection
** may include information about usage by each LAN client
** may include other data
* accepts requests from `router driver` to disable WAN interfaces

=== router driver

* Polls router for status
* Writes status data to database (backend)
* Polls backend to determine if an interface has exceeded data allowance:
** If any interface has data remaining: turn off interfaces that have exceeded limit
** Else: ensure all interfaces are on

=== database

Stores router status data.

=== REST service

Handles the following:

* Receives router status data from `router driver`
* Writes router data to database
* Handles `web server` requests (ultimately to present data to client)
* Handles requests from notifications service

=== web server

Serves web data to client.

=== web frontend

Collection of front-end files to be served to web client by web server.

=== web client

System/user that is viewing hotspot Internet data via web browser.

=== notifications

Performs following functions:

* Ingests usage emails from cellular provider (_e.g._ Verizon Wireless)
** Posts info of usage emails to `REST service`
* Polls REST service
** Initiates sending of notifications (based on results of polling)

=== email delivery service

Likely third-party service for sending notification e-mails to subscribed users.

=== usage email recipient

Inbox to which cellular provider has been configured to send usage emails.
For example, it may be possible to configure the cellular account to send
emails to a specified email address when data usage reaches 90% and/or 100%
of allowed data usage for the billing cycle. Although `router driver` is polling
the router (for usage information at a higher sampling rate), these usage emails
provide the most accurate information about how much data remains.

== [[projects]] Projects

The following projects exist in this `hotspot_internet` group. Where applicable, the
corresponding component of the <<hierarchical-diagram, hierarchical diagram>> is shown
in parentheses. You can find source code and design documents for those projects
in the https://gitlab.com/hotspot_internet[hotspot_internet group].

* parent (_i.e._ this project)
* tlr600 (`router driver`)
** name is the router model (TP-Link device) for the current use case
* rest-service (`REST service`)
* web-frontend (`web frontend`)
* notifications (`notifications`)
